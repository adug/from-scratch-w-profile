# ADUG Composer Install Demo

## Create a new project *from scratch*

1. Create a directory where your new package will live

    ```
    $ mkdir MY_NEW_PROJECT
    ```

2. Change directory to your new package

    ```
    $ cd MY_NEW_PROJECT
    ```

2. Initialize new Composer project

    ```
    $ composer init
    ```

    1. hit <kbd>enter</kbd> to accept the default

    ```
    Package name (<vendor>/<name>) [root/MY_NEW_PROJECT]:
    ```

    2. enter a description for your project `*`

    ```
    Description []:
    ```

    3. enter your author information `*`

    ```
    Author [FIRST_NAME LAST_NAME <EMAIL@DOMAIN.com>, n to skip]:
    ```

    4. enter your minimum stability choice (we choose alpha `**`)

    ```
    Minimum Stability []: alpha
    ```

    5. enter the package type here (we choose project `***`)

    ```
    Package Type (e.g. library, project, metapackage, composer-plugin) []: project
    ```

    6. license type for the package `*`

    ```
    License []:
    ```
    7. Insert more steps

    8. Wait.. :fingers_crossed: :coffee:

    `*` These settings are for informational purposes within your automatically generated composer.json file

    `**` Put relevent information about stability choices and may a link here

## Installation Instructions
1. Clone the repository

    ```
    $ git clone git@gitlab.com:adug/from-scratch.git MY_NEW_PROJECT
    ```

2. Change directory to your local copy

    ```
    $ cd MY_NEW_PROJECT
    ```

3. Install the project (will install Drupal as a dependency)

    ```
    ../MY_NEW_PROJECT$ composer install
    ```